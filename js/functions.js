$(document).ready(function(){
  $('.restart').click(function(){
    keyPress("refresh");
  });
  
  sprite = document.getElementById("blocks");	
	sprite.onload = function(){
		init();
	}
	$(window).focus();
  
  function init(){	
  	tile_width = 45; //Width of each tile
  	tile_height = 45; //Height of each tile
  	game_x = 46; //x coord where the game board starts
  	game_y = 46; //y coord where the game board starts
  	game_width = 450; //width of the game board
  	game_height = 900; //height of the game board
  	speed = 500; //base number of milliseconds that it takes a tile to move down 1 tile
  	multiplier = 1.15; //Every level increase the speed by this multiplier
  	//level_points = 750; //# of points needed to get to the next level
  	line_value = 100;
  	drop_block_points = 1;
  	total_lines = 0; // Total number of lines cleared
  	lines_in_level = 10; // How many lines need to be cleared in each level
  	level_lines = lines_in_level; // How many lines needed to go to the first level
  	can_move = true;
  	paused = false;
  	game_over = false;
  	
  	score = 0;
  	level = 1;
  	
  	canvas = document.getElementById("board"); //Initiatie the Canvas element
  	canvas = canvas.getContext("2d"); //Set the context for the Canvas
  	canvas.drawImage(sprite, 0, 0, 860, 945, 0, 0, 860, 945); //Add the background image
  	canvas.fillStyle = "#d93250";
  	canvas.font = "40px Helvetica, Arial, Sans-Serif";
  	timer = window.setInterval(dropBlock,speed); //Set the timer for dropping the blocks
  }
  
  function keyPress(key) {
    var can_move = true;
    if(key == "left"){ // Left
  		if(validateMove(current_block.shape, current_block.row, current_block.col -1)){
    		drawBoard();
    		current_block.col = current_block.col -1;
    		drawBlock(current_block.col, current_block.row);
  		}
    } else if(key == "right"){ // Right
  		if(validateMove(current_block.shape, current_block.row, current_block.col +1)){
        drawBoard();
    		current_block.col = current_block.col +1;
    		drawBlock(current_block.col, current_block.row); 
      }
  	} else if(key == "up"){ // Up
  		rotateBlock();
  	} else if(key == "down"){ // Down
  		if(typeof(current_block) !== "undefined" && current_block != null){
    		if(validateMove(current_block.shape, current_block.row, current_block.col)){
      		addScore(0,drop_block_points);
    		}
      }
      dropBlock();
  	} else if(key == "hardDown"){ // Hard Down
      hard_down = true;
  		while(hard_down) {
    		dropBlock();
  		}
    } else if(key == "pause"){ // Down
  		pause();
  	} else if(key == "refresh"){ // Down
  		restart();
  	} else if(key == "select"){ // Down
  		if(game_over) {
    		restart();
  		}
  	}
  }
  
  $(window).keydown(function(e) {	
  	if(e.keyCode == 37 || e.charCode == 37){ // Left
  		keyPress("left");
  	} else if(e.keyCode == 39 || e.charCode == 39){ // Right
  		keyPress("right");
  	} else if(e.keyCode == 38 || e.charCode == 38){ // Up
  		keyPress("up");
  	} else if(e.keyCode == 40 || e.charCode == 40){ // Down
  		keyPress("down");
  	} else if(e.keyCode == 32 || e.charCode == 32){ // "space"
  		keyPress("hardDown");
  	} else if(e.keyCode == 80 || e.charCode == 80){ // "p"
  		keyPress("pause");
  	} else if(e.keyCode == 82 || e.charCode == 82){ // "r"
  		keyPress("refresh");
  	}
  });
  
  function validateMove(shape, start_row, start_col){
    if(paused) {
      return false;
    }
    can_move = true;
    $.each(shape, function(row_key, row_value){
      $.each(row_value, function(col_key, value){
        if(value > 0 && board[start_row + row_key][start_col + col_key] != 0) {
          can_move = false;
          return false;
        }
      })
    })
    return can_move;
  }
  
  function drawBlock(col, row){
  	//$('.row span').html(row);
  	//$('.col span').html(col);
  	
  	tile_x = col * tile_width + game_x;
  	tile_y = row * tile_height + game_y;
  	if(typeof current_block.background_x == "number"){
  		$.each(current_block.shape, function(key, value){
  			$.each(value, function(key, value){
  				if(value != 0){
  					canvas.drawImage(sprite, current_block.background_x, current_block.background_y, tile_width, tile_height, tile_x, tile_y, tile_width, tile_height);
  				}	
  				tile_x = tile_x + tile_width;	
  			});
  			tile_x = col * tile_width + game_x;
  			tile_y = tile_y + tile_height;
  		});
  	}
  }
  
  function drawNextBlock(){
    if(next_block.shape[0].length == 2){
      col = 12.75;
      row = 11;
    } else if(next_block.shape[0].length == 3){
      col = 12.75;
      row = 11; 
    } else if(next_block.shape[0].length == 4){
      col = 12.75;
      row = 10.5;
    }
    
    canvas.drawImage(sprite, tile_width*col, tile_width*11, tile_width*5.2, tile_height*4.2, tile_width*col, tile_height*11, tile_width*5.2, tile_height*4.2); // clear next block container
    
    tile_x = col * tile_width + game_x;
  	tile_y = row * tile_height + game_y;
  	if(typeof next_block.background_x == "number"){
  		$.each(next_block.shape, function(key, value){
  			$.each(value, function(key, value){
  				if(value != 0){
  					canvas.drawImage(sprite, next_block.background_x, next_block.background_y, tile_width, tile_height, tile_x, tile_y, tile_width, tile_height);
  				}	
  				tile_x = tile_x + tile_width;	
  			});
  			tile_x = col * tile_width + game_x;
  			tile_y = tile_y + tile_height;
  		});
  	}
  }
  
  function dropBlock(){
  	//Set a new block if we don't currently have one
  	if(typeof(current_block) == "undefined" || current_block == null){
  		hard_down = false;
  		current_block = getNewBlock();
  	}
  	
  	col = current_block.col;
  	row = current_block.row;
  	
  	//Check if the block can move down
  	can_move = true;
  	row_orig = row;
  	if(row < 0){
  		row = 0;
  	}
  	
  	i = 0;
  	$.each(current_block.shape, function(key, value){
  		$.each(value, function(key,value){
  			if(value == 1){
  				if(typeof(board[row +i]) == "undefined" || board[row +i][col + key] != 0){
  					can_move = false;
  				} 
  			}
  		});
  		i++;
  		//alert(board[row +i]);
  	});
  	row = row_orig;
  	
  
  	
  	if(can_move){
  	
  		// This block can move down, so we need to draw it and increase the current row
  	
  		if(row == -1){ //New block dropping		
  			row = 0; // We want to add the bottom of the block to the top row of the board
  			tile_x = col * tile_width + game_x; // x coord of the col
  			tile_y = row * tile_height + game_y; // y coord of the row
  			
  			last_line = current_block.shape[current_block.shape.length - 1];
  			$.each(last_line, function(key, value){
  				if(value == 1){
  					canvas.drawImage(sprite, current_block.background_x, current_block.background_y, tile_width, tile_height, tile_x, tile_y, tile_width, tile_height);
  				}
  				tile_x = tile_x + tile_width;
  			});
  			row = 1 - current_block.shape.length; 
  		} else {
  			cleanBlock(col, row - 1);
  			drawBlock(col, row, tile_type);
  		}
  		current_block.row++;
  		addScore(0, drop_block_points);
  		
  		
  	} else {
  		
  		if(row < 0){
  			timer = window.clearInterval(timer);
  			endGame();
  			return false;
  		}
  		
  		
  		//Write the final spot for the block into the board object
  		row = row - 1; // Call to dropBlock happens 
  		i = 0;
  		$.each(current_block.shape, function(key, value){
  			$.each(value, function(key,value){
  				if(value != 0){
  					board[row + i][col + key] = current_block.type;
  				}
  			});
  			i++;
  		});
  		checkLines();
  		drawBoard();
  		current_block = null;
  	}
  	//addDebug();
  }
  
  function cleanBlock(col, row){
  	drawBoard();
  	/*
  	tile_y = row * tile_height + game_y;
  	$.each(current_block.shape, function(row_key, value){
  		tile_x = col * tile_width + game_x;
  		$.each(value, function(col_key,value){
  		  if(row + row_key >= 0) {
    			if(value > 0 && board[row + row_key][col + col_key] === 0 && tile_x >= game_x && tile_y >= game_y){ // if there was a tile from the current block there and there wasn't a game piece there and it's within the bounds
    				canvas.drawImage(sprite, game_x, game_y, tile_width, tile_height, tile_x, tile_y, tile_width, tile_height); // draw a default tile
    			}
  			}
  			tile_x = tile_x + tile_width;
  		});
  		tile_y = tile_y + tile_height;
  	});
  	*/
  }
  
  function getNewBlock(){
  	if(typeof(next_block) == "undefined" || next_block == null){
  		// Set next_block
  		tile_type = Math.floor(Math.random() * (objCount(blocks) -1)) +1;
    	next_block = jQuery.extend(true, {}, blocks[tile_type]);
    	next_block.row = -1;
    	next_block.col = next_block.start_col;
    	next_block.type = tile_type;
    	
    	// Set current_block
    	tile_type = Math.floor(Math.random() * (objCount(blocks) -1)) +1;
    	current_block = jQuery.extend(true, {}, blocks[tile_type]);
    	current_block.row = -1;
    	current_block.col = current_block.start_col;
    	current_block.type = tile_type;
  	} else {
    	current_block = next_block;
    	
    	// Set next_block
  		tile_type = Math.floor(Math.random() * (objCount(blocks) -1)) +1;
    	next_block = jQuery.extend(true, {}, blocks[tile_type]);
    	next_block.row = -1;
    	next_block.col = next_block.start_col;
    	next_block.type = tile_type;
  	}
  	
  	drawNextBlock();
  	
  	return current_block;
  }
  
  function endGame(){
    game_over = true;
  	$('#game-over').fadeIn();
  }
  
  function pause() {
    if(typeof(paused) != "undefined" && paused) {
      paused = false;
      can_move = true;
      timer = window.setInterval(dropBlock,speed); //Set the timer for dropping the blocks
    } else {
      paused = true;
      can_move = false;
      clearInterval(timer);
    }
  }
  
  function restart(){
    $('#game-over').fadeOut();
    clearInterval(timer);
    current_block = getNewBlock();
    $.each(board, function(row, value){
      $.each(value, function(col,value){
        board[row][col] = 0;
      })
    })
    init();
  }
  
  function objCount(obj){
  	count = 0;
  	$.each(obj, function(key, value){
  		count++;
  	});
  	return count;
  }
  
  function addDebug(){
  	$('#debug').show();
  	$('table#board').html('');
  	$.each(board, function(key, value){
  		table_row = key;
  		$('table#board').append('<tr class="row-' + table_row + '"></tr>');
  		$.each(value, function(key, value){
  			$('tr.row-' + table_row).append("<td>" + value + "</td>");
  		})
  	})
  }
  
  function rotateBlock(){
  	drawBoard();
  	new_array = [];
  	height = current_block.shape.length;
  	width = current_block.shape[0].length;
  	
  	$.each(current_block.shape, function(key, value){
  		i = key;
  		$.each(value, function(key, value){
  			if(typeof(new_array[key]) == "undefined"){
  				new_array[key] = [];
  			}
  			new_array[key][height -i -1] = value;
  		});
  	});
  	
  	new_row = current_block.row;
  	new_col = current_block.col;
  	
    if(validateMove(new_array, new_row, new_col)){
    	current_block.shape = new_array;
    	
    	/*
    	if(current_block.type == 3) {
    	  console.log(current_block.shape);
        if(current_block.shape[1][0] === 1){
          current_block.col = new_col + 1;
          console.log('1');
        } else {
          current_block.col = new_col - 1;
          console.log('2')
        }
    	}
    	*/
    	
    	drawBlock(current_block.col, current_block.row);
    } else {
      drawBlock(current_block.col, current_block.row);
    }
  }
  
  function drawBoard(){
  	$.each(board, function(key, value){
  		tile_y = key * tile_height + game_y;
  		$.each(value, function(key, value){
  			tile_x = key * tile_width + game_x;
  			canvas.drawImage(sprite, blocks[value].background_x, blocks[value].background_y, tile_width, tile_height, tile_x, tile_y, tile_width, tile_height);
  		})
  	})
  }
  
  function checkLines(){
  	complete_rows = [];
  	$.each(board, function(key, value){
  		complete = true;
  		$.each(value, function(key, value){
  			if(value == 0){
  				complete = false;
  			}
  		});
  		if(complete == true){
  			complete_rows.push(key);
  		}
  	});
  	
  	var num_complete = complete_rows.length;
  	
  	if(typeof(complete_rows) != "undefined"){
  		$.each(complete_rows, function(key, value){
  			board.splice(value,1);
  			board.unshift([0,0,0,0,0,0,0,0,0,0]);
  		})
  	}
  	delete complete_rows;
  	
  	addScore(num_complete);
  }
  
  Number.prototype.toNearest = function(num) { // num is an exponent of 10
  	return Math.round(this/num)*num;
  }
  
  function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  
  function addScore(lines, points){
  	total_lines = total_lines + lines;
  	
  		score = score + lines * line_value * level;
  	
  	if(points > 0) {
    	score = score + points;
  	}
  	
  	//alert(score.toNearest(level_points));
  	
  	if(total_lines >= level_lines){
  		// Add a level
  		level = level +1;
  		speed = speed / multiplier;
  		line_value = line_value * multiplier;
  		level_lines += lines_in_level;
  		clearInterval(timer);
  		timer = window.setInterval(dropBlock,speed);
  	}
  	/*
  	console.log(total_lines);
  	console.log(level_lines);
  	console.log(level);
  	*/
  	
  	
  	// Score
  	canvas.drawImage(sprite, 615, 87, 200, 60, 615, 87, 200, 60);
  	canvas.fillText(commaSeparateNumber(Math.floor(score)), 615, 127);
  	
  	canvas.drawImage(sprite, 615, 221, 200, 60, 615, 221, 200, 60);
  	canvas.fillText(level, 615, 261);
  	
  	canvas.drawImage(sprite, 615, 357, 200, 60, 615, 357, 200, 60);
  	canvas.fillText(total_lines, 615, 397);
  }
  EXM.system.initialize(function () {
    function handleEXMKey(key) {
      switch (key) {
        case "left":
          keyPress("left");
          break;
        case "right":
          keyPress("right");
          break;
        case "up":
          keyPress("up");
          break;
        case "down":
          keyPress("down");
          break;
        case "select":
          keyPress("select");
          break;
      }
    };
    
    EXM.remote.watchButtons(handleEXMKey);
  });

});