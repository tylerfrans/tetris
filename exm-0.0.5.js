(function (window) {

	var HOST = null;
	var HOST_ORIGIN = null;
	var HOST_CONFIG = null;

	var SDK = {};
	SDK.system = {};
	SDK.remote = {};
	SDK.endpoint = {};

	var callbacks = {
		remote: {
			keypress: function () {}
		},
		system: {
			config: function (details) {
				if (details) {
					HOST_CONFIG = details;
					sendMessage({scope: 'system', action: 'initialize'});
				}
			},
			shudown: function () {}
		}
	};

	SDK.system.initialize = function (callback) {
		checkForConfigMessage();

		// check for config message from host
		function checkForConfigMessage() {
			if (HOST && HOST_ORIGIN && HOST_CONFIG) {
				if (callback) callback();
			} else {
				setTimeout(checkForConfigMessage, 100);
			}
		}
	};

	SDK.system.shutdown = function () {
		sendMessage({scope: 'system', action: 'close'});
	};

	SDK.system.watchShutdown = function (callback) {
		if (callback && typeof(callback) == 'function') callbacks.application.shutdown = callback;
	};

	SDK.endpoint.getType = function () {
		return HOST_CONFIG.device_type;
	};

	SDK.endpoint.getIPAddress = function () {
		return HOST_CONFIG.ip_address;
	};

	SDK.endpoint.getUID = function () {
		return HOST_CONFIG.uid;
	};

	SDK.endpoint.getMACAddress = function () {
		return HOST_CONFIG.mac_address;
	};

	SDK.endpoint.getFirmware = function () {
		return HOST_CONFIG.firmware;
	};

	SDK.endpoint.getBootVersion = function () {
		return HOST_CONFIG.boot_version;
	};

	SDK.endpoint.getWrapperVersion = function () {
		return HOST_CONFIG.wrapper_version;
	};

	SDK.endpoint.getScreenOrientation = function () {
		return HOST_CONFIG.screen_orientation;
	};

	SDK.remote.watchButtons = function (callback) {
		if (callback && typeof(callback) == 'function') {
			callbacks.remote.keypress = function (key) {
				if (key == 'back') sendMessage({scope: 'system', action: 'keepalive'});
				callback(key);
			}
		}
	};

	function receiveMessage(m) {
		if (!m) return false;

		var scope = m.scope;
		var action = m.action;
		var details = m.details;

		if (scope && action && callbacks[scope] && callbacks[scope][action]) callbacks[scope][action](details);
	}

	function sendMessage(message) {
		if (HOST && HOST_ORIGIN && message) {
			HOST.postMessage(message, HOST_ORIGIN);
		}
	}

	window.addEventListener('message', function (e) {
		if (!HOST) HOST = e.source;
		if (!HOST_ORIGIN) HOST_ORIGIN = e.origin;
		if (e.data) receiveMessage(e.data);
	});

	window.onunload = function () {
		sendMessage({
			scope: 'system',
			action: 'navigation'
		});
	};

	window.EXM = SDK;

})(window);